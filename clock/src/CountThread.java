
public class CountThread extends Thread {
	private ClockState clockState;

	public CountThread(ClockState clockState) {
		this.clockState = clockState;
	}

	public void run() {
		int i = 0;
		int j = 0;
		long t0 = System.currentTimeMillis();
		while (true) {
			clockState.tick();
			if (clockState.checkAlarm(i)) {
				clockState.startAlarm();
				i++;
			} else {
				i = 0;
			}

			long now = System.currentTimeMillis();
			long t = t0 + 1000 * (j + 1);
			long sleeptime = t - now;
			j++;
			try {
				Thread.sleep(sleeptime);
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
	}
}