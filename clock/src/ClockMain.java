import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import clock.AlarmClockEmulator;
import clock.io.ClockInput;
import clock.io.ClockInput.UserInput;
import clock.io.ClockOutput;

public class ClockMain {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		AlarmClockEmulator emulator = new AlarmClockEmulator();
		ClockInput in = emulator.getInput();
		ClockOutput out = emulator.getOutput();
		ClockState clockState = new ClockState(out);
		Semaphore s = in.getSemaphore();
		CountThread countThread = new CountThread(clockState);
		countThread.start();

		while (true) {
			s.acquire();
			UserInput userInput = in.getUserInput();
			int choice = userInput.getChoice();
			int h = userInput.getHours();
			int m = userInput.getMinutes();
			int sec = userInput.getSeconds();

			if (choice == 1) {
				clockState.setClockTime(h, m, sec);
			} else if (choice == 2) {
				clockState.setAlarmTime(h, m, sec);
			} else if (choice == 3) {
				clockState.toggleAlarm();
				if (!clockState.getAlarm()) {
					out.setAlarmIndicator(false);
				} else if (clockState.getAlarm()) {
					out.setAlarmIndicator(true);
				}
			}

			System.out.println("choice=" + choice + " h=" + h + " m=" + m + " s=" + sec);
		}
	}
}
