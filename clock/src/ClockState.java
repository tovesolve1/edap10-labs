import java.util.concurrent.Semaphore;

import clock.io.ClockOutput;

public class ClockState {
	private int hour;
	private int minute;
	private int second;
	private int alarmHour;
	private int alarmMinute;
	private int alarmSecond;
	private Semaphore s = new Semaphore(1);
	private boolean alarmOn;
	private ClockOutput out;

	public ClockState(ClockOutput out) {
		this.setClockTime(0, 0, 0);
		this.setAlarmTime(0, 0, 0);
		this.out = out;
	}

	public void setClockTime(int h, int m, int s) {
		try {
			this.s.acquire();
			this.hour = h;
			this.minute = m;
			this.second = s;
			this.s.release();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	public void setAlarmTime(int h, int m, int s) {
		try {
			this.s.acquire();
			this.alarmHour = h;
			this.alarmMinute = m;
			this.alarmSecond = s;
			this.s.release();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	public boolean getAlarm() {
		try {
			this.s.acquire();
			boolean a = this.alarmOn;
			this.s.release();
			return a;
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	public void toggleAlarm() {
		try {
			this.s.acquire();
			if (alarmOn) {
				this.alarmOn = false;
			} else if (!alarmOn) {
				this.alarmOn = true;
			}
			this.s.release();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	public void tick() {
		try {
			this.s.acquire();
			int totalSeconds = hour * 3600 + minute * 60 + second + 1;
			int left = totalSeconds % 3600;
			int h = (totalSeconds - left) / 3600;
			int s = left % 60;
			int m = (left - s) / 60;

			if (h == 24) {
				hour = 0;
				minute = 0;
				second = 0;
			} else {
				hour = h;
				minute = m;
				second = s;
			}
			out.displayTime(hour, minute, second);
			this.s.release();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	public boolean checkAlarm(int i) {
		try {
			this.s.acquire();
			boolean b = (alarmOn
					&& ((hour == alarmHour && minute == alarmMinute && alarmSecond == second) || (0 < i && i < 20)));
			this.s.release();
			return b;
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	public void startAlarm() {
		try {
			this.s.acquire();
			out.alarm();
			this.s.release();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

}
