package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;

/**
 * Program 1 for washing machine.
 */
public class WashingProgram2 extends ActorThread<WashingMessage> {

	private WashingIO io;
	private ActorThread<WashingMessage> temp;
	private ActorThread<WashingMessage> water;
	private ActorThread<WashingMessage> spin;

	public WashingProgram2(WashingIO io, ActorThread<WashingMessage> temp, ActorThread<WashingMessage> water,
			ActorThread<WashingMessage> spin) {
		this.io = io;
		this.temp = temp;
		this.water = water;
		this.spin = spin;
	}

	@Override
	public void run() {
		try {
			System.out.println("washing program 2 started");
			io.lock(true);
			System.out.println("hatch locked");
			
			//Start prewash
			water.send(new WashingMessage(this, WashingMessage.WATER_FILL, 10.0));
			WashingMessage ackW0 = receive();
			System.out.println("Water filled");
			
			temp.send(new WashingMessage(this, WashingMessage.TEMP_SET, 40.0));
			WashingMessage ackT0 = receive();
			System.out.println("Temp set");
			
			spin.send(new WashingMessage(this, WashingMessage.SPIN_SLOW));
			WashingMessage ackS0 = receive();
			System.out.println("Spin set to slow");
			Thread.sleep(20 * 60000 / Settings.SPEEDUP);
			
			temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
			System.out.println("Temp off");
			WashingMessage ackT1 = receive();

			spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
			WashingMessage ackS1 = receive();
			System.out.println("Spin off");
			
			water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
			WashingMessage ackW1 = receive();			
			System.out.println("Water drain");
			
			water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
			System.out.println("Water idle");
	
			
			//start mainwash
			water.send(new WashingMessage(this, WashingMessage.WATER_FILL, 10.0));
			WashingMessage ackW2 = receive();
			System.out.println("Water filled");
			
			temp.send(new WashingMessage(this, WashingMessage.TEMP_SET, 60.0));
			WashingMessage ackT2 = receive();
			System.out.println("Temp set");
			
			//spin for 30min
			spin.send(new WashingMessage(this, WashingMessage.SPIN_SLOW));
			WashingMessage ackS2 = receive();
			System.out.println("Spin set to slow");
			Thread.sleep(30 * 60000 / Settings.SPEEDUP);
			
			temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
			System.out.println("Temp off");
			WashingMessage ackT3 = receive();

			spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
			WashingMessage ackS3 = receive();
			System.out.println("Spin off");
			
			water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
			WashingMessage ackW3 = receive();			
			System.out.println("Water drain");
			
			water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
			System.out.println("Water idle");

			//Rising 5 times
			for(int i = 0; i < 5; i++) {
				water.send(new WashingMessage(this, WashingMessage.WATER_FILL, 10.0));
				WashingMessage ackW4 = receive();
				spin.send(new WashingMessage(this, WashingMessage.SPIN_SLOW));
				WashingMessage ackS4 = receive();
				Thread.sleep(2 * 60000 / Settings.SPEEDUP);
				water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
				WashingMessage ackW5 = receive();
				water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
			}
			
			//Cetrifuging
			water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
			WashingMessage ackW6 = receive();
			System.out.println("Water drain");
			spin.send(new WashingMessage(this, WashingMessage.SPIN_FAST));
			WashingMessage ackS5 = receive();
			System.out.println("Spinfast");
			Thread.sleep(5 * 60000 / Settings.SPEEDUP);
			
			water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
			System.out.println("Water idle");
			spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
			WashingMessage ackS6 = receive();
			System.out.println("Spin off");

			io.lock(false);
			System.out.println("washing program 2 finished");
		} catch (InterruptedException e) {
			// If we end up here, it means the program was interrupt()'ed:
			// set all controllers to idle
			temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
			water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
			spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
			System.out.println("washing program 2 terminated");
		}
	}
}
