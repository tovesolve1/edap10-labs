package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;

public class WaterController extends ActorThread<WashingMessage> {
	public static final int WATER_IDLE = 6; // stop filling/draining, and stop water level regulation
	public static final int WATER_FILL = 7; // fill water to given value (liters)
	public static final int WATER_DRAIN = 8; // drain water from barrel
	private WashingIO io;

	public WaterController(WashingIO io) {
		this.io = io;
	}

	@Override
	public void run() {
		System.out.println("water running");
		WashingMessage n = null;
		try {
			while (true) {
				// wait for up to 5 (simulated) seconds for a WashingMessage
				WashingMessage m = receiveWithTimeout(5000 / Settings.SPEEDUP);

				// if m is null, it means a minute passed and no message was received
				if (m != null) {
					n = m;
				}

				if (n != null) {
					if (n.getCommand() == WATER_IDLE) {
						io.fill(false);
						io.drain(false);
						n = null;
					} else if (n.getCommand() == WATER_FILL) {
						io.drain(false);
						io.fill(true);
						if (io.getWaterLevel() > 10) {
							io.fill(false);
							n.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
							n = null;
						}
					} else if (n.getCommand() == WATER_DRAIN) {
						io.fill(false);
						io.drain(true);
						if (io.getWaterLevel() <= 0) {
							//io.drain(false);
							n.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
							n = null;
						}
					}
				}
			}
		} catch (

		InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
