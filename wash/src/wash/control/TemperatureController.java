package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;

public class TemperatureController extends ActorThread<WashingMessage> {
	private WashingIO io;
	private double currentTemp;
	private double goalTemp;
	double lowerBound;
	double upperBound;
	public static final int TEMP_IDLE = 4; // stop heater, and stop temperature regulation
	public static final int TEMP_SET = 5; // regulate temperature to given value (degrees C)
	private boolean finished;
	private static final double mu = 0.678;
	private static final double m1 = 0.20952;
	private boolean isIdle;

	public TemperatureController(WashingIO io) {
		this.io = io;
		this.currentTemp = io.getTemperature();
		this.goalTemp = currentTemp;
		this.lowerBound = goalTemp - 2 + m1;
		this.upperBound = goalTemp - mu;
	}

	@Override
	public void run() {
		try {
			boolean start = false;
			WashingMessage n = null;

			while (true) {
				// wait for up to 10 (simulated) seconds for a WashingMessage
				WashingMessage m;
				m = receiveWithTimeout(10000 / Settings.SPEEDUP);

				// if m is null, it means a minute passed and no message was received
				if (m != null) {
					n = m;
				}

				if (!isIdle) {
					currentTemp = io.getTemperature();
					if (currentTemp < lowerBound) {
						io.heat(true);
					} else if (currentTemp > upperBound) {
						io.heat(false);
					}
				}

				if (n != null) {
					if (n.getCommand() == TEMP_SET) {
						goalTemp = n.getValue();
						lowerBound = goalTemp - 2 + m1;
						upperBound = goalTemp - mu;
						currentTemp = io.getTemperature();
						isIdle = false;
						if (lowerBound < currentTemp && currentTemp < upperBound) {
							n.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
							io.heat(false);
							n = null;
						}

					} else if (n.getCommand() == TEMP_IDLE) {
						isIdle = true;
						io.heat(false);
						n.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
						n = null;
					}
				}
			}

		} catch (

		InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO
	}
}
