package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;

public class SpinController extends ActorThread<WashingMessage> {
	public static final int SPIN_OFF = 1; // stop barrel from rotating
	public static final int SPIN_SLOW = 2; // rotate barrel slowly, alternating left/right
	public static final int SPIN_FAST = 3; // rotate barrel fast
	public WashingIO io;
	private int spinState = io.SPIN_LEFT;

	public SpinController(WashingIO io) {
		this.io = io;
	}

	@Override
	public void run() {
		try {
			boolean start = false;

			while (true) {
				// wait for up to a (simulated) minute for a WashingMessage
				WashingMessage m = receiveWithTimeout(60000 / Settings.SPEEDUP);

				// if m is null, it means a minute passed and no message was received
				if (m != null) {
					start = true;

					if (m.getCommand() == SPIN_OFF) {
						start = false;
						io.setSpinMode(io.SPIN_IDLE);
					} else if (m.getCommand() == SPIN_SLOW) {
						if (spinState == io.SPIN_LEFT) {
							io.setSpinMode(io.SPIN_RIGHT);
							spinState = io.SPIN_RIGHT;
						} else {
							io.setSpinMode(io.SPIN_LEFT);
							spinState = io.SPIN_LEFT;
						}
					} else if (m.getCommand() == SPIN_FAST) {
						start = false;
						io.setSpinMode(io.SPIN_FAST);
					}
					m.getSender().send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
				}

				if (start) {
					if (spinState == io.SPIN_LEFT) {
						io.setSpinMode(io.SPIN_RIGHT);
						spinState = io.SPIN_RIGHT;
					} else if (spinState == io.SPIN_RIGHT) {
						io.setSpinMode(io.SPIN_LEFT);
						spinState = io.SPIN_LEFT;
					}
				}

			}
		} catch (InterruptedException unexpected) {
			System.out.println("catch");
			// we don't expect this thread to be interrupted,
			// so throw an error if it happens anyway
			throw new Error(unexpected);
		}
	}
}
