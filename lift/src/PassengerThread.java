import java.util.Random;

import lift.LiftView;
import lift.Passenger;

public class PassengerThread extends Thread {
	private LiftView view;
	private Monitor m;

	public PassengerThread(LiftView view, Monitor m) {
		this.view = view;
		this.m = m;

	}

	public void run() {

		while (true) {
			try {
				Passenger pass = view.createPassenger();
				int fromFloor = pass.getStartFloor();
				int toFloor = pass.getDestinationFloor();
				Random rand = new Random();

				// Obtain a number between [0 - 45].
				int delay = rand.nextInt(46)*1000;
				Thread.sleep(delay);
				
				pass.begin();
				m.addWaitingPassenger(fromFloor);
				m.enteringLift(fromFloor, toFloor, pass); //critical sections
				pass.enterLift();
				m.decreaseMovingPass();
				
				m.exitingLift(toFloor, pass); //critical sections
				pass.exitLift();
				m.decreaseMovingPass();
				pass.end();
				
			} catch (InterruptedException e) {
				System.out.println("error in passenger Thread");
				e.printStackTrace();
			}

		}

	}

}
