import lift.LiftView;

public class LiftThread extends Thread {
	private LiftView view;
	private Monitor m;

	public LiftThread(LiftView view, Monitor m) {
		this.view = view;
		this.m = m;
	}

	public void run() {
		try {
			int currentFloor = 0;
			int nextFloor = 0;
			while (true) {
				m.stopMoving(nextFloor);
				nextFloor = m.startMoving(currentFloor, nextFloor);
				view.moveLift(currentFloor, nextFloor);
				currentFloor = nextFloor;

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
