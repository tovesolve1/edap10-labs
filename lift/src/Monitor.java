import lift.LiftView;
import lift.Passenger;

public class Monitor {
	private int floor = 0; // the floor the lift is currently on
	private boolean moving = true; // true if the lift is moving, false if standing still with doors open
	private int direction = 1; // +1 if lift is going up, -1 if going down
	private int[] waitEntry; // number of passengers waiting to enter the lift at the various floors
	private int[] waitExit; // number of passengers (in lift) waiting to leave at the various floors
	private int load = 0; // number of passengers currently in the lift
	private LiftView view;
	private int totalPass;
	private int movingPass;

	public Monitor(LiftView view) {
		this.view = view;
		waitEntry = new int[7];
		waitExit = new int[7];
		for (int i = 0; i < 7; i++) {
			waitEntry[i] = 0;
			waitExit[i] = 0;
		}

	}

	public synchronized int startMoving(int fromFloor, int toFloor) throws InterruptedException {		
		while (!(waitExit[fromFloor] == 0 && (waitEntry[fromFloor] == 0 || load == 4) && movingPass == 0 && totalPass != 0)) {
			view.showDebugInfo(waitEntry, waitExit);
			wait();
		}
		if (!moving) {
			view.closeDoors();
		}
		moving = true;
		if (floor == 6) {
			direction = -1;
		}
		if (floor == 0) {
			direction = 1;
		}
		floor += direction;

		notifyAll();
		return floor;
	}

	public synchronized void stopMoving(int toFloor) throws InterruptedException {
		while (!((toFloor == floor))) {
			wait();
		}

		if (waitEntry[toFloor] != 0 || waitExit[toFloor] != 0) {
			view.openDoors(floor);
			moving = false;
		}
		notifyAll();
	}


	public synchronized void addWaitingPassenger(int arrivingFloor) {
		waitEntry[arrivingFloor] += 1;
		totalPass++;
		notifyAll();
	}

	public synchronized void enteringLift(int fromFloor, int toFloor, Passenger pass) throws InterruptedException {
		while (!(!moving && floor == fromFloor && load < 4)) {
			wait();
		}
		movingPass++;
		waitExit[toFloor] += 1;
		waitEntry[fromFloor] -= 1;
		load++;
		notifyAll();
	}

	public synchronized void exitingLift(int toFloor, Passenger pass) throws InterruptedException {
		while (!(!moving && floor == toFloor)) {
			wait();
		}
		movingPass++;
		waitExit[toFloor] -= 1;
		load--;
		totalPass--;
		notifyAll();
	}

	public synchronized void decreaseMovingPass() {
		movingPass--;
		notifyAll();
	}

}
