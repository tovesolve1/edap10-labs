import java.util.ArrayList;

import lift.LiftView;
import lift.Passenger;

public class RideLift {

	public static void main(String[] args) {
		LiftView view = new LiftView();
		Monitor m = new Monitor(view);
		Thread lift = new LiftThread(view, m);

		ArrayList<PassengerThread> passengerList = new ArrayList<PassengerThread>();

		for (int i = 0; i < 20; i++) {
			passengerList.add(new PassengerThread(view, m));
		}

		for (PassengerThread p : passengerList) {
			p.start();
		}

		lift.start();

	}

}
