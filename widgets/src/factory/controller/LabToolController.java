package factory.controller;

import java.util.concurrent.Semaphore;

import factory.model.DigitalSignal;
import factory.model.WidgetKind;
import factory.simulation.Painter;
import factory.simulation.Press;
import factory.swingview.Factory;

/**
 * Implementation of the ToolController interface, to be used for the Widget
 * Factory lab.
 * 
 * @see ToolController
 */
public class LabToolController implements ToolController {
	private final DigitalSignal conveyor, press, paint;
	private final long pressingMillis, paintingMillis;
	private boolean pressRunning = false;
	private boolean paintRunning = false;
	private Semaphore sem = new Semaphore(2);

	public LabToolController(DigitalSignal conveyor, DigitalSignal press, DigitalSignal paint, long pressingMillis,
			long paintingMillis) {
		this.conveyor = conveyor;
		this.press = press;
		this.paint = paint;
		this.pressingMillis = pressingMillis;
		this.paintingMillis = paintingMillis;
	}

	@Override
	public synchronized void onPressSensorHigh(WidgetKind widgetKind) throws InterruptedException {
		//
		// TODO: you will need to modify this method.
		//
		// Note that this method can be called concurrently with onPaintSensorHigh
		// (that is, in a separate thread).
		//
		if (widgetKind == WidgetKind.BLUE_RECTANGULAR_WIDGET) {
			//pressRunning = true;
			sem.acquire();
			turnOff();
			press.on();
			waitOutside(pressingMillis);
			press.off();
			waitOutside(pressingMillis); // press needs this time to retract
			//pressRunning = false;
			sem.release();
			turnOn();
		}
	}

	@Override
	public synchronized void onPaintSensorHigh(WidgetKind widgetKind) throws InterruptedException {
		//
		// TODO: you will need to modify this method.
		//
		// Note that this method can be called concurrently with onPressSensorHigh
		// (that is, in a separate thread).
		//
		if (widgetKind == WidgetKind.ORANGE_ROUND_WIDGET) {
			//paintRunning = true;
			sem.acquire();
			turnOff();
			paint.on();
			waitOutside(paintingMillis);
			paint.off();
			//paintRunning = false;
			sem.release();
			turnOn();
		}
	}

	/** Helper method: wait outside of monitor for 'millis' milliseconds. */
    private void waitOutside(long millis) throws InterruptedException {
	    long timeToWakeUp = System.currentTimeMillis() + millis;
	    long currentTime = System.currentTimeMillis();
	    
	    while ( timeToWakeUp > currentTime) {
	    	long dt = timeToWakeUp - currentTime;
	    	wait(dt);
	    	currentTime = System.currentTimeMillis();
	    }
    }

	private void turnOn() {
//		if (!pressRunning && !paintRunning) {
//			conveyor.on();
//		}
		if (sem.availablePermits() == 2) {
			conveyor.on();
		}
	}

	private void turnOff() {
//		if (pressRunning || paintRunning) {
//			conveyor.off();
//		}
		if (sem.availablePermits() !=2) {
			conveyor.off();
		}
	}

	// -----------------------------------------------------------------------

	public static void main(String[] args) {
		Factory factory = new Factory();
		ToolController toolController = new LabToolController(factory.getConveyor(), factory.getPress(),
				factory.getPaint(), Press.PRESSING_MILLIS, Painter.PAINTING_MILLIS);
		factory.startSimulation(toolController);
	}
}
