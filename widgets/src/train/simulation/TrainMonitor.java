package train.simulation;

import java.util.HashSet;
import java.util.Set;
import train.model.Segment;

public class TrainMonitor {
	private Set<Segment> busySegments;
	
	public TrainMonitor() {
		this.busySegments = new HashSet<Segment>();
	}
	
	
	public synchronized void removeSegment(Segment s) {
		busySegments.remove(s);
		notifyAll();
	}

	public synchronized void addSegment(Segment s) throws InterruptedException {
		while (busySegments.contains(s)) {
			wait();
		}
		busySegments.add(s);
	}

}
