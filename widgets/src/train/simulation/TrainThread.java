package train.simulation;

import java.util.LinkedList;

import train.model.Route;
import train.model.Segment;
import train.view.TrainView;

public class TrainThread extends Thread {
	private TrainView view;
	private TrainMonitor tm;

	public TrainThread(TrainView view, TrainMonitor tm) {
		this.tm = tm;
		this.view = view;
	}

	public void run() {
		try {
			Route route = view.loadRoute();
			LinkedList<Segment> segments = new LinkedList<Segment>();
			for (int i = 0; i < 3; i++) {
				Segment s = route.next();
				tm.addSegment(s);
				s.enter();
				segments.addFirst(s);
				//segments.get(i).enter();
			}

			while (true) {
				Segment head = route.next();
				tm.addSegment(head);
				head.enter();
				segments.addFirst(head);
				Segment tail = segments.removeLast();
				tail.exit();
				tm.removeSegment(tail);
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}