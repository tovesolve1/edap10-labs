package train.simulation;

import java.util.ArrayList;
import train.view.TrainView;

public class TrainSimulation {

	public static void main(String[] args) {
		TrainView view = new TrainView();
		TrainMonitor tm = new TrainMonitor();
		ArrayList<TrainThread> trainList = new ArrayList<TrainThread>();

		for (int i = 0; i < 20; i++) {
			trainList.add(new TrainThread(view, tm));
		}

		for (TrainThread t : trainList) {
			t.start();
		}
	}
}
