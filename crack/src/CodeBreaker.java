import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import client.view.ProgressItem;
import client.view.StatusWindow;
import client.view.WorklistItem;
import network.Sniffer;
import network.SnifferCallback;
import rsa.Factorizer;
import rsa.ProgressTracker;

public class CodeBreaker implements SnifferCallback {

	private final JPanel workList;
	private final JPanel progressList;

	private final JProgressBar mainProgressBar;
	private static ExecutorService pool;

	// -----------------------------------------------------------------------

	private CodeBreaker() {
		StatusWindow w = new StatusWindow();
		workList = w.getWorkList();
		progressList = w.getProgressList();
		mainProgressBar = w.getProgressBar();
		pool = Executors.newFixedThreadPool(2);
		w.enableErrorChecks();

	}

	// -----------------------------------------------------------------------

	public static void main(String[] args) {

		/*
		 * Most Swing operations (such as creating view elements) must be performed in
		 * the Swing EDT (Event Dispatch Thread).
		 * 
		 * That's what SwingUtilities.invokeLater is for.
		 */

		SwingUtilities.invokeLater(() -> {
			CodeBreaker codeBreaker = new CodeBreaker();
			new Sniffer(codeBreaker).start();
		});

	}

	// -----------------------------------------------------------------------

	/** Called by a Sniffer thread when an encrypted message is obtained. */
	@Override
	public void onMessageIntercepted(String message, BigInteger n) {
		SwingUtilities.invokeLater(() -> {
			WorklistItem wlitem = new WorklistItem(n, message);
			JButton button = new JButton();
			button.setText("Break");

			button.addActionListener(e -> {
				workList.remove(wlitem);
				onBreakButtonPressed(message, n);
			});
			wlitem.add(button);
			workList.add(wlitem);

		});
	}

	public void onBreakButtonPressed(String message, BigInteger n) {
		SwingUtilities.invokeLater(() -> {
			JButton removeButton = new JButton();
			removeButton.setText("Remove");
			JButton cancelButton = new JButton();
			cancelButton.setText("Cancel");

			ProgressItem pitem = new ProgressItem(n, message);
			ProgressTracker tracker = new Tracker(pitem, mainProgressBar);
			progressList.add(pitem);

			removeButton.addActionListener(ev -> onRemoveButtonPressed(pitem));

			Runnable crackingTask = () -> {
				try {
					String s = Factorizer.crack(message, n, tracker);
					SwingUtilities.invokeLater(() -> {
						pitem.getTextArea().setText(s);
						pitem.remove(cancelButton);
						pitem.add(removeButton);
					});

				} catch (InterruptedException e1) {
					//e1.printStackTrace();
				}
			};

			Future<?> future = pool.submit(crackingTask);
			mainProgressBar.setMaximum(mainProgressBar.getMaximum() + 1000000);

			cancelButton.addActionListener(ev -> {
				future.cancel(true);
				onCancelButtonPressed(pitem);
				pitem.remove(cancelButton);
				pitem.add(removeButton);
			});

			pitem.add(cancelButton);
		});

	}

	public void onCancelButtonPressed(ProgressItem pitem) {
		SwingUtilities.invokeLater(() -> {
		pitem.getTextArea().setText("[cancelled]");
		int currentValue = pitem.getProgressBar().getValue();
		
			pitem.getProgressBar().setValue(1000000);
			mainProgressBar.setValue(mainProgressBar.getValue() + (1000000 - currentValue));
		});

	}

	public void onRemoveButtonPressed(ProgressItem pitem) {
		SwingUtilities.invokeLater(() -> {
		progressList.remove(pitem);
		mainProgressBar.setValue(mainProgressBar.getValue() - 1000000);
		mainProgressBar.setMaximum(mainProgressBar.getMaximum() - 1000000);
		});
	}

	private static class Tracker implements ProgressTracker {
		private int totalProgress = 0;
		private ProgressItem pitem;
		private JProgressBar mainProgressBar;

		public Tracker(ProgressItem pitem, JProgressBar mainProgressBar) {
			this.pitem = pitem;
			this.mainProgressBar = mainProgressBar;

		}

		/**
		 * Called by Factorizer to indicate progress. The total sum of ppmDelta from all
		 * calls will add upp to 1000000 (one million).
		 * 
		 * @param ppmDelta portion of work done since last call, measured in ppm (parts
		 *                 per million)
		 */
		@Override
		public void onProgress(int ppmDelta) {
			totalProgress += ppmDelta;
			SwingUtilities.invokeLater(() -> {
				pitem.getProgressBar().setValue(totalProgress);
				mainProgressBar.setValue(mainProgressBar.getValue() + ppmDelta);
			});
		}
	}
}
